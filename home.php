<?php   session_start();  ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>

    <style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        div{
            position: absolute;
            top: 50%;
            left: 50%;
            margin-right: -50%;
            transform: translate(-50%, -50%); 

            background-color: black;
            width: 50%;
            height: 40vh;
            color: white;

            display: flex;
            flex-direction: column;
            justify-content:space-around;
            align-items: center;
        }

        h1{
            font-size: 80px;
            font-weight: bold;
            color: darkcyan;
            margin: 0;
            padding: 0;
            text-align: center;
        }

        form{
            display:flex;
            flex-direction:column;
            justify-content:space-around;
            align-items:center;
        
            height: 30vh;
        }

        p{
            font-size: 20px;
            font-weight: bold;
            font-style: italic;
            color: red;
            margin: 0;
            padding: 0;
            text-align: center;
        }

        button {
            padding: 10px;
            margin: 10px 0px;
            background-color: darkcyan;
            color: white;
            font-weight: bold;
            width: 50%;
            border: none;
        }
        button:hover{
            transform: scale(1.1);
        }
    </style>
</head>
<body>
  <div>
    <h1>Welcome</h1>
        <form method="POST" action="./server.php">
        <p><?php echo $_SESSION['use']; ?></p>
        <button type="submit" name="logout">Logout</button>
        </form>
  </div>
   
</body>
</html>



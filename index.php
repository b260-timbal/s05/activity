<?php session_start();?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S05: Activity</title>

    <style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        body {
            /* background-color:black; */
            width:100%;
        }

        div {
            position: absolute;
            top: 50%;
            left: 50%;
            margin-right: -50%;
            transform: translate(-50%, -50%); 

            background-color: black;
            width: 30%;
            height: 40vh;
            color: white;

            display: flex;
            justify-content: center;
            align-items: center;
        
        }
        form{
            display: flex;
            flex-direction: column;
            justify-content: space-between;
            align-items: center;
            width: 100%;
            height: 30vh;
        }
        h1{
            color: darkcyan;
            padding: 20px;
        }
        input{
            height: 25px;
        }
        input:focus{
            outline: none;
        }
        button {
            padding: 10px;
            margin: 10px 0px;
            background-color: darkcyan;
            color: white;
            font-weight: bold;
            width: 20%;
            border: none;
        }
        button:hover{
            transform: scale(1.1);
        }
        p{
            color: red;
        }
            
        
    </style>

</head>
<body>
    <div>
        <form method="POST" action="./server.php">
        <input type="hidden" name="action" value="login"/>
            <h1>Login</h1>
            <h3>Email</h3>
            <input type="text" name="email" placeholder="Email" required>

            <h3>Password</h3>
            <input type="password" name="password" placeholder="Password" required>
            <?php ?>
            <button type="submit" name="login">Login</button>
            
        </form>
    </div>
</body>
</html>